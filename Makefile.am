# -*-makefile-*-
##############################################################################
#
# This file contains original work by Andy Southgate.  Contact details can be
# found at http://www.mushware.com.  This file was placed in the Public
# Domain by Andy Southgate and Mushware Limited in 2004.
#
# This software carries NO WARRANTY of any kind.
#
##############################################################################

#
# $Id: app.Makefile.am,v 1.14 2007/06/30 16:02:14 southa Exp $
# $Log: app.Makefile.am,v $
# Revision 1.14  2007/06/30 16:02:14  southa
# Generic packaging
#
# Revision 1.13  2007/06/30 11:45:46  southa
# X11 release
#
# Revision 1.12  2007/06/27 19:54:16  southa
# X11 release
#
# Revision 1.11  2007/06/27 15:39:59  southa
# X11 packaging
#
# Revision 1.10  2007/06/27 12:58:34  southa
# Debian packaging
#
# Revision 1.9  2007/06/26 16:27:51  southa
# X11 tweaks
#
# Revision 1.8  2007/06/25 15:59:49  southa
# X11 compatibility
#
# Revision 1.7  2007/06/14 14:59:14  southa
# win32 fixes
#
# Revision 1.6  2007/06/05 12:15:16  southa
# Level 21
#
# Revision 1.5  2007/06/02 15:57:02  southa
# Shader fix and prerelease work
#
# Revision 1.4  2005/08/01 20:23:43  southa
# Backdrop and build fixes
#
# Revision 1.3  2005/06/22 20:02:00  southa
# MushGame link work
#
# Revision 1.2  2005/06/04 13:18:46  southa
# Updates for Mac OS Release 0.1.2
#
# Revision 1.1  2004/01/07 11:14:15  southa
# Snapshot fixes
#

AUTOMAKE_OPTIONS=1.6
SUBDIRS = @TOP_SUBDIRS@
DIST_SUBDIRS = @TOP_SUBDIRS@
RUNFILE=${DESTDIR}${bindir}/adanaxisgpl
RUNFILESAFE=${DESTDIR}${bindir}/adanaxisgpl-recover
GZIPFILE=adanaxisgpl-@APP_VERSION@.tar.gz
ORIGGZIPFILE=adanaxisgpl-@APP_VERSION@.orig.tar.gz
BZIPFILE=adanaxisgpl-@APP_VERSION@.tar.bz2
ICONSFILE=adanaxisgpl-icons.tar.bz2

DATAMAKEFILE=data-adanaxisgpl/Makefile.am
MUSHDATADIR=data-adanaxisgpl

all:prebuild-script

prebuild-script:
	-$(SHELL) -c 'if [ -f prebuild.sh ];then $(SHELL) ./prebuild.sh;fi'

release: install-strip
	(cd $(top_srcdir);$(SHELL) MakeRelease.sh)

mac-release: $(GZIPFILE)
	(cd $(top_srcdir);$(SHELL) macosx/MakeRelease.sh 'Adanaxis GPL' 'adanaxisgpl' '@APP_VERSION@' '$(top_srcdir)/macosx/project/build/Deployment' '$(top_srcdir)/data-adanaxis' 1)
	(cd $(top_srcdir);$(SHELL) macosx/MakeRelease.sh 'Adanaxis GPL' 'adanaxisgpl' '@APP_VERSION@' '$(top_srcdir)/macosx/project/build/Deployment' '$(top_srcdir)/data-adanaxis' 0)

win32-release: $(GZIPFILE)
	(cd $(top_srcdir);$(SHELL) win32/MakeInstaller.sh 'Adanaxis GPL' 'adanaxisgpl' '@APP_VERSION@' '$(top_srcdir)/VisualC/project/Release' '$(top_srcdir)/data-adanaxis' 1)
	(cd $(top_srcdir);$(SHELL) win32/MakeInstaller.sh 'Adanaxis GPL' 'adanaxisgpl' '@APP_VERSION@' '$(top_srcdir)/VisualC/project/Release' '$(top_srcdir)/data-adanaxis' 0)

$(GZIPFILE): dist

$(BZIPFILE):$(GZIPFILE)
	rm -f $(BZIPFILE)
	gunzip -c $(GZIPFILE) | bzip2 --best > $(BZIPFILE)

rpm:adanaxisgpl.spec $(GZIPFILE)
# cp $(ICONSFILE) ~/rpm/SOURCES/$(ICONSFILE)
	cp $(GZIPFILE) ~/rpm/SOURCES/$(GZIPFILE)
	rpmbuild -ba $<

debian-release: $(GZIPFILE)
	cp -f $(GZIPFILE) ../$(ORIGGZIPFILE)
	chmod +x debian/rules
	dpkg-buildpackage -rfakeroot

debian-release-unsigned: $(GZIPFILE)
	cp -f $(GZIPFILE) ../$(ORIGGZIPFILE)
	chmod +x debian/rules
	dpkg-buildpackage -rfakeroot -us -uc

# Blank line required - configuration scripts append here
EXTRA_DIST=autogen.pl prebuild.sh adanaxisgpl.spec macosx/project/TesseractTrainer.plist macosx/project/Adanaxis-Info.plist macosx/project/all.Info.plist macosx/project/MushRuby-Info.plist macosx/project/TestMustl-Info.plist macosx/project/MushPie-Info.plist macosx/project/TestMushMesh-Info.plist macosx/project/TestMushPie-Info.plist macosx/project/dummp-Info.plist macosx/project/Mushcore-Info.plist macosx/project/TestMushcore-Info.plist macosx/project/Mustl-Info.plist macosx/project/Maurheen-Info.plist macosx/project/MushMesh-Info.plist README.win32 README.linux README README.macosx SourceConditioner.pl autogen.pl SourceProcess.pm macosx/project/project.xcodeproj/project.pbxproj macosx/project/project.xcodeproj/southa.pbxuser macosx/MakeRelease.sh macosx/adanaxis_app.icns scripts/AmendToType.rb scripts/FileMush.rb scripts/ImageProcess.rb scripts/MushObject.rb scripts/ProcessAnimation.rb scripts/ProcessCosmos.rb scripts/ProcessIntern.rb scripts/ProcessMush.rb scripts/SourceProcess.rb targets/adanaxis.spec.generic targets/adanaxis.debian.watch targets/adanaxis.debian.menu targets/adanaxis.spec.mdk targets/adanaxis.spec.fedora targets/adanaxis.ChangeLog targets/adanaxis.debian.changelog targets/adanaxis.debian.copyright targets/adanaxis.def targets/adanaxis.debian.control targets/adanaxis.debian.rules targets/app.prebuild.sh targets/app.COPYING targets/sdl_app.spec.mdk targets/sdl_app.configure.in targets/app.INSTALL targets/app.AUTHORS targets/app.README targets/app.Makefile.am targets/app.acinclude.m4 targets/sdl_app.spec targets/commercial.COPYING targets/empty.prebuild.sh targets/empty.MushSecretKeys.cpp targets/sdl_app.spec.mdk targets/sdl_app.configure.in targets/sdl_app.spec targets/win32.installer.nsi win32/adanaxiscontroldemo_uninst_app.ico win32/adanaxisrenderingdemo_app.ico win32/adanaxiscontroldemo_app.ico win32/adanaxisrenderingdemo_uninst_app.ico win32/adanaxiscontroldemo_inst_app.ico win32/adanaxisrenderingdemo_inst_app.ico win32/MakeInstaller.sh win32/installer.nsi win32/FileListToNSI.rb VisualC/project/adanaxis.vcproj VisualC/project/adanaxis.rc VisualC/project/adanaxis_app.ico VisualC/project/adanaxiscontroldemo_app.ico x11/icons/adanaxisgpl-32.png x11/icons/adanaxisgpl-16.png x11/icons/adanaxisgpl-48.png x11/icons/adanaxisgpl32x32.xpm x11/icons/adanaxisgpl48x48.xpm x11/icons/adanaxisgpl16x16.xpm 
dist_man_MANS=x11/man/adanaxisgpl.6 
