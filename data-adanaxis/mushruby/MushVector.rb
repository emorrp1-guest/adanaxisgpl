#%Header {
##############################################################################
#
# File data-adanaxis/mushruby/MushVector.rb
#
# Author Andy Southgate 2006-2007
#
# This file contains original work by Andy Southgate.  The author and his
# employer (Mushware Limited) irrevocably waive all of their copyright rights
# vested in this particular version of this file to the furthest extent
# permitted.  The author and Mushware Limited also irrevocably waive any and
# all of their intellectual property rights arising from said file and its
# creation that would otherwise restrict the rights of any party to use and/or
# distribute the use of, the techniques and methods used herein.  A written
# waiver can be obtained via http://www.mushware.com/.
#
# This software carries NO WARRANTY of any kind.
#
##############################################################################
#%Header } mu3tJbD6CEVt4dpEcmHUBA
# $Id: MushVector.rb,v 1.4 2007/03/13 21:45:04 southa Exp $
# $Log: MushVector.rb,v $
# Revision 1.4  2007/03/13 21:45:04  southa
# Release process
#
# Revision 1.3  2006/08/01 17:21:14  southa
# River demo
#
# Revision 1.2  2006/08/01 13:41:08  southa
# Pre-release updates
#

# This class wraps the C++ class MushMeshRubyVector
class MushVector

# Class: MushVector
#
# Description:
#
# This object contains a vector with 4 floating-point elements.
#
# Method: new
#
# Creates a new vector object.  
#
# Parameters:
#
# x, y, z, w - Values for the vector
#
# Returns:
#
# New MushVector object.
#
# Default:
#
# The default constructor leaves the values *uninitialised*.
#
# Example:
#
# (example)
# vec = MushVector.new(1, 2, 3.1, 4.5);
# (end)
#
# Group: Links
#- Wrapper file:doxygen/class_mush_mesh_ruby_vector.html
#- Implemetation file:doxygen/class_mush_mesh_vector.html

end
