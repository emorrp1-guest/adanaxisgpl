#%Header {
##############################################################################
#
# File data-adanaxis/ruby/AdanaxisAIKhazi.rb
#
# Copyright Andy Southgate 2006-2007
#
# This file may be used and distributed under the terms of the Mushware
# Software Licence version 1.4, under the terms for 'Proprietary original
# source files'.  If not supplied with this software, a copy of the licence
# can be obtained from Mushware Limited via http://www.mushware.com/.
# One of your options under that licence is to use and distribute this file
# under the terms of the GNU General Public Licence version 2.
#
# This software carries NO WARRANTY of any kind.
#
##############################################################################
#%Header } aZmOtgfuS1g+kQm04rgIbw
# $Id: AdanaxisAIKhazi.rb,v 1.6 2007/06/27 13:18:52 southa Exp $
# $Log: AdanaxisAIKhazi.rb,v $
# Revision 1.6  2007/06/27 13:18:52  southa
# Debian packaging
#
# Revision 1.5  2007/06/27 12:58:09  southa
# Debian packaging
#
# Revision 1.4  2007/04/18 09:21:51  southa
# Header and level fixes
#
# Revision 1.3  2007/03/27 14:01:02  southa
# Attendant AI
#
# Revision 1.2  2007/03/13 21:45:06  southa
# Release process
#
# Revision 1.1  2006/09/30 13:46:32  southa
# Seek and patrol
#

require 'Mushware.rb'
require 'AdanaxisAI.rb'

class AdanaxisAIKhazi < AdanaxisAI

end
