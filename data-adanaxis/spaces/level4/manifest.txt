#%Header {
##############################################################################
#
# File data-adanaxis/spaces/level4/manifest.txt
#
# Copyright Andy Southgate 2006-2007
#
# This file may be used and distributed under the terms of the Mushware
# Software Licence version 1.4, under the terms for 'Proprietary original
# source files'.  If not supplied with this software, a copy of the licence
# can be obtained from Mushware Limited via http://www.mushware.com/.
# One of your options under that licence is to use and distribute this file
# under the terms of the GNU General Public Licence version 2.
#
# This software carries NO WARRANTY of any kind.
#
##############################################################################
#%Header } YGBz4aZIyrAljWsTf7zD/g
# $Id: manifest.txt,v 1.3 2007/06/27 12:58:18 southa Exp $
# $Log: manifest.txt,v $
# Revision 1.3  2007/06/27 12:58:18  southa
# Debian packaging
#
# Revision 1.2  2007/04/18 09:21:55  southa
# Header and level fixes
#
# Revision 1.1  2007/03/27 15:34:42  southa
# L4 and carrier ammo
#

directory='level4'
name='Lone Artillery'
creator='Andy Southgate (www.mushware.com)'
permit='level3'
next='level5'
