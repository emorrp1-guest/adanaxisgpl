#%Header {
##############################################################################
#
# File data-adanaxis/spaces/level17/manifest.txt
#
# Copyright Andy Southgate 2006-2007
#
# This file may be used and distributed under the terms of the Mushware
# Software Licence version 1.4, under the terms for 'Proprietary original
# source files'.  If not supplied with this software, a copy of the licence
# can be obtained from Mushware Limited via http://www.mushware.com/.
# One of your options under that licence is to use and distribute this file
# under the terms of the GNU General Public Licence version 2.
#
# This software carries NO WARRANTY of any kind.
#
##############################################################################
#%Header } fOKlywoRZdE7VS0H7ooXtQ
# $Id: manifest.txt,v 1.2 2007/06/27 12:58:15 southa Exp $
# $Log: manifest.txt,v $
# Revision 1.2  2007/06/27 12:58:15  southa
# Debian packaging
#
# Revision 1.1  2007/05/24 15:13:50  southa
# Level 17
#
# Revision 1.1  2007/05/09 19:24:43  southa
# Level 14
#

directory='level17'
name='Factory'
creator='Andy Southgate (www.mushware.com)'
permit='level16'
next='level18'
