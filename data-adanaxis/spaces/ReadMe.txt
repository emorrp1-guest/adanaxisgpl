Adanaxis 'spaces' directory
===========================

It is possible to use new mods/levels/spaces by dragging their directories into
this one.  However the script files in those directories will have full access
to your system (to delete and remove files, etc.) so make sure that they come
from a trusted source.  The win32 and X11 uninstallation process won't remove
the new directories.

Andy Southgate 2006-08-01



#%Header {
##############################################################################
#
# File data-adanaxis/spaces/ReadMe.txt
#
# Author Andy Southgate 2006-2007
#
# This file contains original work by Andy Southgate.  The author and his
# employer (Mushware Limited) irrevocably waive all of their copyright rights
# vested in this particular version of this file to the furthest extent
# permitted.  The author and Mushware Limited also irrevocably waive any and
# all of their intellectual property rights arising from said file and its
# creation that would otherwise restrict the rights of any party to use and/or
# distribute the use of, the techniques and methods used herein.  A written
# waiver can be obtained via http://www.mushware.com/.
#
# This software carries NO WARRANTY of any kind.
#
##############################################################################
#%Header } FlD4qi7VTh/i01dnJHlRwA
