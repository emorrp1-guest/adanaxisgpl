#%Header {
##############################################################################
#
# File data-adanaxis/spaces/level23/manifest.txt
#
# Copyright Andy Southgate 2006-2007
#
# This file may be used and distributed under the terms of the Mushware
# Software Licence version 1.4, under the terms for 'Proprietary original
# source files'.  If not supplied with this software, a copy of the licence
# can be obtained from Mushware Limited via http://www.mushware.com/.
# One of your options under that licence is to use and distribute this file
# under the terms of the GNU General Public Licence version 2.
#
# This software carries NO WARRANTY of any kind.
#
##############################################################################
#%Header } 9AHHPUltko8qti4aWGISdg
# $Id: manifest.txt,v 1.2 2007/06/27 12:58:17 southa Exp $
# $Log: manifest.txt,v $
# Revision 1.2  2007/06/27 12:58:17  southa
# Debian packaging
#
# Revision 1.1  2007/06/06 15:11:20  southa
# Level 23
#

directory='level23'
name='Stealth Mines'
creator='Andy Southgate (www.mushware.com)'
permit='level22'
next='level24'
