#%Header {
##############################################################################
#
# File data-adanaxis/system/start_safe.txt
#
# Author Andy Southgate 2006-2007
#
# This file contains original work by Andy Southgate.  The author and his
# employer (Mushware Limited) irrevocably waive all of their copyright rights
# vested in this particular version of this file to the furthest extent
# permitted.  The author and Mushware Limited also irrevocably waive any and
# all of their intellectual property rights arising from said file and its
# creation that would otherwise restrict the rights of any party to use and/or
# distribute the use of, the techniques and methods used herein.  A written
# waiver can be obtained via http://www.mushware.com/.
#
# This software carries NO WARRANTY of any kind.
#
##############################################################################
#%Header } R8/ugb35Y6igOXz1PVLdAQ
# $Id: start_safe.txt,v 1.4 2007/06/25 17:58:46 southa Exp $
# $Log: start_safe.txt,v $
# Revision 1.4  2007/06/25 17:58:46  southa
# X11 fixes
#
# Revision 1.3  2007/04/18 09:21:57  southa
# Header and level fixes
#
# Revision 1.2  2006/08/03 13:49:58  southa
# X11 release work
#
# Revision 1.1  2005/06/13 17:34:52  southa
# Adanaxis creation
#
# Revision 1.1  2005/06/03 15:43:10  southa
# Created
#

load($SYSTEM_PATH+'/start.txt')
configset('SAFE_MODE', 1)
configset('X11_USE_SDL_LISTMODES', 0)
