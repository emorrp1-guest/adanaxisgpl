//%includeGuardStart {
#ifndef MUSHGAMEDIALOGUE_H
#define MUSHGAMEDIALOGUE_H
//%includeGuardStart } JpQ+4mjSjf/kETbpLlCPYA
//%Header {
/*****************************************************************************
 *
 * File: src/MushGame/MushGameDialogue.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } SZS4xbzRg9K58IMuI4xfUQ
/*
 * $Id: MushGameDialogue.h,v 1.4 2007/04/18 09:22:39 southa Exp $
 * $Log: MushGameDialogue.h,v $
 * Revision 1.4  2007/04/18 09:22:39  southa
 * Header and level fixes
 *
 * Revision 1.3  2006/07/31 11:01:38  southa
 * Music and dialogues
 *
 * Revision 1.2  2006/06/01 15:39:21  southa
 * DrawArray verification and fixes
 *
 * Revision 1.1  2005/06/14 20:39:41  southa
 * Adanaxis work
 *
 */

#include "MushGameStandard.h"

#include "API/mushGame.h"

typedef GameDialogue MushGameDialogue;

//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
