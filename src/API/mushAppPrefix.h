//%includeGuardStart {
#ifndef MUSHAPPPREFIX_H
#define MUSHAPPPREFIX_H
//%includeGuardStart } qRfZvzLMQJYXPXj7oZCrVQ
//%Header {
/*****************************************************************************
 *
 * File: src/API/mushAppPrefix.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } FePturihrLdNw2epS+S1gw
/*
 * $Id: mushAppPrefix.h,v 1.3 2007/04/18 09:21:57 southa Exp $
 * $Log: mushAppPrefix.h,v $
 * Revision 1.3  2007/04/18 09:21:57  southa
 * Header and level fixes
 *
 * Revision 1.2  2006/06/01 15:38:44  southa
 * DrawArray verification and fixes
 *
 * Revision 1.1  2005/07/01 10:03:30  southa
 * Projection work
 *
 */

// For use as precompiled header for application builds

#include "mushGL.h"
#include "mushMushcore.h"
#include "mushMushcoreSTL.h"
#include "mushMushcoreIO.h"

//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
