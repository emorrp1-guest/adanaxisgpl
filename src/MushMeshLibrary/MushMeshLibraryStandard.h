//%includeGuardStart {
#ifndef MUSHMESHLIBRARYSTANDARD_H
#define MUSHMESHLIBRARYSTANDARD_H
//%includeGuardStart } BO73H62/Cu9evAzBLHqM0w
//%Header {
/*****************************************************************************
 *
 * File: src/MushMeshLibrary/MushMeshLibraryStandard.h
 *
 * Copyright: Andy Southgate 2005-2007
 *
 * This file may be used and distributed under the terms of the Mushware
 * Software Licence version 1.4, under the terms for 'Proprietary original
 * source files'.  If not supplied with this software, a copy of the licence
 * can be obtained from Mushware Limited via http://www.mushware.com/.
 * One of your options under that licence is to use and distribute this file
 * under the terms of the GNU General Public Licence version 2.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } ZmcQwhYDlg1VfWBX8Tc7FQ
/*
 * $Id: MushMeshLibraryStandard.h,v 1.6 2007/06/27 12:58:30 southa Exp $
 * $Log: MushMeshLibraryStandard.h,v $
 * Revision 1.6  2007/06/27 12:58:30  southa
 * Debian packaging
 *
 * Revision 1.5  2007/04/18 09:22:54  southa
 * Header and level fixes
 *
 * Revision 1.4  2006/08/01 17:21:36  southa
 * River demo
 *
 * Revision 1.3  2006/06/01 15:39:34  southa
 * DrawArray verification and fixes
 *
 * Revision 1.2  2005/08/02 17:11:28  southa
 * win32 build fixes
 *
 * Revision 1.1  2005/07/12 20:39:05  southa
 * Mesh library work
 *
 */

#include "API/mushMushcore.h"
#include "API/mushMushMesh.h"

//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
