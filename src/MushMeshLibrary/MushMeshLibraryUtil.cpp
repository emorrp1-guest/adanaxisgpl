//%Header {
/*****************************************************************************
 *
 * File: src/MushMeshLibrary/MushMeshLibraryUtil.cpp
 *
 * Copyright: Andy Southgate 2005-2007
 *
 * This file may be used and distributed under the terms of the Mushware
 * Software Licence version 1.4, under the terms for 'Proprietary original
 * source files'.  If not supplied with this software, a copy of the licence
 * can be obtained from Mushware Limited via http://www.mushware.com/.
 * One of your options under that licence is to use and distribute this file
 * under the terms of the GNU General Public Licence version 2.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } N1Yg7MJfd0jZdHMKFuCc6Q
/*
 * $Id: MushMeshLibraryUtil.cpp,v 1.7 2007/06/27 12:58:30 southa Exp $
 * $Log: MushMeshLibraryUtil.cpp,v $
 * Revision 1.7  2007/06/27 12:58:30  southa
 * Debian packaging
 *
 * Revision 1.6  2007/04/18 09:22:54  southa
 * Header and level fixes
 *
 * Revision 1.5  2006/08/01 17:21:36  southa
 * River demo
 *
 * Revision 1.4  2006/06/01 15:39:34  southa
 * DrawArray verification and fixes
 *
 * Revision 1.3  2005/08/01 17:58:44  southa
 * Object explosion
 *
 * Revision 1.2  2005/07/27 18:10:06  southa
 * Collision checking
 *
 * Revision 1.1  2005/07/19 13:44:26  southa
 * MushMesh4Chunk work
 *
 */

#include "MushMeshLibraryUtil.h"

