//%includeGuardStart {
#ifndef MUSHCOREXMLSTREAM_H
#define MUSHCOREXMLSTREAM_H
//%includeGuardStart } GhGsa+DKBbuQEbZ85tykwA
//%Header {
/*****************************************************************************
 *
 * File: src/Mushcore/MushcoreXMLStream.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } boxggCAztdCEZBew1a3EGg
/*
 * $Id: MushcoreXMLStream.h,v 1.5 2007/04/18 09:23:15 southa Exp $
 * $Log: MushcoreXMLStream.h,v $
 * Revision 1.5  2007/04/18 09:23:15  southa
 * Header and level fixes
 *
 * Revision 1.4  2006/06/01 15:39:49  southa
 * DrawArray verification and fixes
 *
 * Revision 1.3  2005/05/19 13:02:17  southa
 * Mac release work
 *
 * Revision 1.2  2004/01/02 21:13:14  southa
 * Source conditioning
 *
 * Revision 1.1  2003/09/21 09:51:10  southa
 * Stream autogenerators
 *
 */

#include "MushcoreStandard.h"
#include "MushcoreObject.h"

class MushcoreXMLStream : public MushcoreObject
{
    
};
//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
