//%Header {
/*****************************************************************************
 *
 * File: src/Mushcore/MushcoreObject.cpp
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } GGyndtcbyh9fJLvCikAuGw
/*
 * $Id: MushcoreObject.cpp,v 1.5 2007/04/18 09:23:11 southa Exp $
 * $Log: MushcoreObject.cpp,v $
 * Revision 1.5  2007/04/18 09:23:11  southa
 * Header and level fixes
 *
 * Revision 1.4  2006/06/01 15:39:45  southa
 * DrawArray verification and fixes
 *
 * Revision 1.3  2005/05/19 13:02:16  southa
 * Mac release work
 *
 * Revision 1.2  2004/01/02 21:13:13  southa
 * Source conditioning
 *
 * Revision 1.1  2003/09/21 09:51:09  southa
 * Stream autogenerators
 *
 */

#include "MushcoreObject.h"
