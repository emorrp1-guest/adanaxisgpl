//%includeGuardStart {
#ifndef MUSHMESHRUBYBASEWORLDSPHERE_H
#define MUSHMESHRUBYBASEWORLDSPHERE_H
//%includeGuardStart } Pkrtf+IGTkYCf3H19XjOHQ
//%Header {
/*****************************************************************************
 *
 * File: src/MushMeshRuby/MushMeshRubyBaseWorldSphere.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } XIIFQQXD3xWYxtbZLiE1+g
/*
 * $Id: MushMeshRubyBaseWorldSphere.h,v 1.2 2007/04/18 09:22:55 southa Exp $
 * $Log: MushMeshRubyBaseWorldSphere.h,v $
 * Revision 1.2  2007/04/18 09:22:55  southa
 * Header and level fixes
 *
 * Revision 1.1  2006/09/12 15:28:50  southa
 * World sphere
 *
 */

#include "MushMeshRubyStandard.h"

class MushMeshRubyBaseWorldSphere : public MushRubyObj<MushMeshLibraryWorldSphere>
{
public:
	static void RubyInstall(void);
	
private:
};

//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
