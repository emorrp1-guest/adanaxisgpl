//%includeGuardStart {
#ifndef MUSHMESHRUBYBASESINGLEFACET_H
#define MUSHMESHRUBYBASESINGLEFACET_H
//%includeGuardStart } RP9RrfF4P8ExsJns+5k9gw
//%Header {
/*****************************************************************************
 *
 * File: src/MushMeshRuby/MushMeshRubyBaseSingleFacet.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } p7CfelikCM3etXmnNDZQ9w
/*
 * $Id: MushMeshRubyBaseSingleFacet.h,v 1.2 2007/04/18 09:22:55 southa Exp $
 * $Log: MushMeshRubyBaseSingleFacet.h,v $
 * Revision 1.2  2007/04/18 09:22:55  southa
 * Header and level fixes
 *
 * Revision 1.1  2006/07/17 14:43:41  southa
 * Billboarded deco objects
 *
 */

#include "MushMeshRubyStandard.h"

class MushMeshRubyBaseSingleFacet : public MushRubyObj<MushMeshLibrarySingleFacet>
{
public:
	static void RubyInstall(void);
	
private:
};
//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
