//%includeGuardStart {
#ifndef MUSHGLAPPHANDLER_H
#define MUSHGLAPPHANDLER_H
//%includeGuardStart } vlc6QkGIBynej6KwLCz6Gw
//%Header {
/*****************************************************************************
 *
 * File: src/MushGL/MushGLAppHandler.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } aRAn1a3mQ5T7C3mLAMC28w
/*
 * $Id: MushGLAppHandler.h,v 1.5 2007/04/18 09:22:33 southa Exp $
 * $Log: MushGLAppHandler.h,v $
 * Revision 1.5  2007/04/18 09:22:33  southa
 * Header and level fixes
 *
 * Revision 1.4  2006/06/30 15:05:32  southa
 * Texture and buffer purge
 *
 * Revision 1.3  2006/06/01 15:39:16  southa
 * DrawArray verification and fixes
 *
 * Revision 1.2  2005/07/06 19:08:26  southa
 * Adanaxis control work
 *
 * Revision 1.1  2005/07/05 13:52:22  southa
 * Adanaxis work
 *
 */

#include "API/mushGL.h"

typedef SDLAppHandler MushGLAppHandler;

//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
