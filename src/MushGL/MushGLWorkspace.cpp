//%Header {
/*****************************************************************************
 *
 * File: src/MushGL/MushGLWorkspace.cpp
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } jPOkW5HjVXwU6RoEjxQUyw
/*
 * $Id: MushGLWorkspace.cpp,v 1.3 2007/04/18 09:22:37 southa Exp $
 * $Log: MushGLWorkspace.cpp,v $
 * Revision 1.3  2007/04/18 09:22:37  southa
 * Header and level fixes
 *
 * Revision 1.2  2006/06/01 15:39:19  southa
 * DrawArray verification and fixes
 *
 * Revision 1.1  2005/07/04 15:59:00  southa
 * Adanaxis work
 *
 */

#include "MushGLWorkspace.h"

