//%includeGuardStart {
#ifndef MUSHGLDRAW_H
#define MUSHGLDRAW_H
//%includeGuardStart } CgU/vbrcygOZPIBiH5rEjg
//%Header {
/*****************************************************************************
 *
 * File: src/MushGL/MushGLDraw.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } VX5AXO/eCXOacubEdLGLOQ
/*
 * $Id: MushGLDraw.h,v 1.1 2007/03/16 19:50:45 southa Exp $
 * $Log: MushGLDraw.h,v $
 * Revision 1.1  2007/03/16 19:50:45  southa
 * Damage indicators
 *
 */

#include "API/mushMushMesh.h"

#include "MushGLStandard.h"

class MushGLDraw
{
public:
    static void QuadsDraw(std::vector<Mushware::t2GLVal> inVertices,
                          std::vector<Mushware::t4GLVal> inColours);
};


//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
