//%includeGuardStart {
#ifndef MUSHFILE_H
#define MUSHFILE_H
//%includeGuardStart } sCkOeLF8Kc9cIQU8PgucNA
//%Header {
/*****************************************************************************
 *
 * File: src/MushFile/MushFile.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } RMhqIenrZGrAEXdsBQvO4Q
/*
 * $Id: MushFile.h,v 1.4 2007/04/18 09:22:31 southa Exp $
 * $Log: MushFile.h,v $
 * Revision 1.4  2007/04/18 09:22:31  southa
 * Header and level fixes
 *
 * Revision 1.3  2006/12/15 18:42:36  southa
 * Secret keys
 *
 * Revision 1.2  2006/11/07 11:08:54  southa
 * Texture loading from mushfiles
 *
 * Revision 1.1  2006/11/06 12:56:31  southa
 * MushFile work
 *
 */

#include "MushFileAccessor.h"
#include "MushFileFile.h"
#include "MushFileFilename.h"
#include "MushFileKeys.h"
#include "MushFileStandard.h"

//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
