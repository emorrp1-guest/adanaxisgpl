//%Header {
/*****************************************************************************
 *
 * File: src/Adanaxis/AdanaxisStandard.cpp
 *
 * Copyright: Andy Southgate 2005-2007
 *
 * This file may be used and distributed under the terms of the Mushware
 * Software Licence version 1.4, under the terms for 'Proprietary original
 * source files'.  If not supplied with this software, a copy of the licence
 * can be obtained from Mushware Limited via http://www.mushware.com/.
 * One of your options under that licence is to use and distribute this file
 * under the terms of the GNU General Public Licence version 2.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } o+fm57FgwxABSDa8zvZGmQ
/*
 * $Id: AdanaxisStandard.cpp,v 1.6 2007/06/27 12:58:26 southa Exp $
 * $Log: AdanaxisStandard.cpp,v $
 * Revision 1.6  2007/06/27 12:58:26  southa
 * Debian packaging
 *
 * Revision 1.5  2007/04/18 09:22:04  southa
 * Header and level fixes
 *
 * Revision 1.4  2006/08/01 17:21:30  southa
 * River demo
 *
 * Revision 1.3  2006/06/01 15:38:49  southa
 * DrawArray verification and fixes
 *
 * Revision 1.2  2005/06/14 13:25:33  southa
 * Adanaxis work
 *
 * Revision 1.1  2005/06/13 17:34:55  southa
 * Adanaxis creation
 *
 */

#include "AdanaxisStandard.h"

