//%includeGuardStart {
#ifndef ADANAXISSTL_H
#define ADANAXISSTL_H
//%includeGuardStart } GsOKDpeL2HVZa6FN+/Z0Kg
//%Header {
/*****************************************************************************
 *
 * File: src/Adanaxis/AdanaxisSTL.h
 *
 * Copyright: Andy Southgate 2005-2007
 *
 * This file may be used and distributed under the terms of the Mushware
 * Software Licence version 1.4, under the terms for 'Proprietary original
 * source files'.  If not supplied with this software, a copy of the licence
 * can be obtained from Mushware Limited via http://www.mushware.com/.
 * One of your options under that licence is to use and distribute this file
 * under the terms of the GNU General Public Licence version 2.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } NDcRGnahce+5YFZq8SwV4A
/*
 * $Id: AdanaxisSTL.h,v 1.7 2007/06/27 12:58:25 southa Exp $
 * $Log: AdanaxisSTL.h,v $
 * Revision 1.7  2007/06/27 12:58:25  southa
 * Debian packaging
 *
 * Revision 1.6  2007/04/18 09:22:04  southa
 * Header and level fixes
 *
 * Revision 1.5  2006/08/01 17:21:28  southa
 * River demo
 *
 * Revision 1.4  2006/06/22 19:07:28  southa
 * Build fixes
 *
 * Revision 1.3  2006/06/01 15:38:48  southa
 * DrawArray verification and fixes
 *
 * Revision 1.2  2005/06/14 13:25:33  southa
 * Adanaxis work
 *
 * Revision 1.1  2005/06/13 17:34:55  southa
 * Adanaxis creation
 *
 */

#if defined(HAVE_CONFIG_H)  && !defined(MUSHWARE_CONFIG_H)
#define MUSHWARE_CONFIG_H 1
#include "config.h"
#endif

#if defined(HAVE_MUSHCORE_MUSHCORE_H)
#include <Mushcore/MushcoreSTL.h>
#elif defined(HAVE_MUSHCORE_H)
#include <MushcoreSTL.h>
#else
#include "Mushcore/MushcoreSTL.h"
#endif
//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
