//%Header {
/*****************************************************************************
 *
 * File: src/MushMesh/MushMeshVector.cpp
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } bDDavMsbLqWBKG7VXmLpag
/*
 * $Id: MushMeshVector.cpp,v 1.7 2007/04/18 09:22:52 southa Exp $
 * $Log: MushMeshVector.cpp,v $
 * Revision 1.7  2007/04/18 09:22:52  southa
 * Header and level fixes
 *
 * Revision 1.6  2006/05/02 17:32:13  southa
 * Texturing
 *
 * Revision 1.5  2005/05/19 13:02:11  southa
 * Mac release work
 *
 * Revision 1.4  2004/01/02 17:31:48  southa
 * MushPie work and XML fixes
 *
 * Revision 1.3  2003/10/15 12:26:59  southa
 * MushMeshArray neighbour testing and subdivision work
 *
 * Revision 1.2  2003/10/14 13:07:25  southa
 * MushMesh vector creation
 *
 * Revision 1.1  2003/10/14 10:46:05  southa
 * MeshMover creation
 *
 */

#include "MushMeshVector.h"

using namespace Mushware;
using namespace std;
