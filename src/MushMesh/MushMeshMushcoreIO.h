//%includeGuardStart {
#ifndef MUSHMESHMUSHCOREIO_H
#define MUSHMESHMUSHCOREIO_H
//%includeGuardStart } 87V0oANPKNQtuQeirgbldw
//%Header {
/*****************************************************************************
 *
 * File: src/MushMesh/MushMeshMushcoreIO.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } y12Kottp1UgP5k5asXm8/w
/*
 * $Id: MushMeshMushcoreIO.h,v 1.6 2007/04/18 09:22:48 southa Exp $
 * $Log: MushMeshMushcoreIO.h,v $
 * Revision 1.6  2007/04/18 09:22:48  southa
 * Header and level fixes
 *
 * Revision 1.5  2006/06/22 19:07:32  southa
 * Build fixes
 *
 * Revision 1.4  2006/06/01 15:39:30  southa
 * DrawArray verification and fixes
 *
 * Revision 1.3  2005/05/19 13:02:10  southa
 * Mac release work
 *
 * Revision 1.2  2004/01/06 20:46:52  southa
 * Build fixes
 *
 * Revision 1.1  2004/01/04 17:02:30  southa
 * MushPie extras and MushcoreIO fixes
 *
 */

#if defined(HAVE_CONFIG_H)  && !defined(MUSHWARE_CONFIG_H)
#define MUSHWARE_CONFIG_H 1
#include "config.h"
#endif

#if defined(HAVE_MUSHCORE_MUSHCORE_H)
#include <Mushcore/MushcoreIO.h>
#elif defined(HAVE_MUSHCORE_H)
#include <MushcoreIO.h>
#else
#include "Mushcore/MushcoreIO.h"
#endif
//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
