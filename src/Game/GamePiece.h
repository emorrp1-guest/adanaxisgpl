//%includeGuardStart {
#ifndef GAMEPIECE_H
#define GAMEPIECE_H
//%includeGuardStart } q9phvipr2Sb7bVhTMJCVdg
//%Header {
/*****************************************************************************
 *
 * File: src/Game/GamePiece.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } /4XqfDYi5tDoM/P7lU3r1A
/*
 * $Id: GamePiece.h,v 1.26 2007/04/18 09:22:13 southa Exp $
 * $Log: GamePiece.h,v $
 * Revision 1.26  2007/04/18 09:22:13  southa
 * Header and level fixes
 *
 * Revision 1.25  2006/06/01 15:38:58  southa
 * DrawArray verification and fixes
 *
 * Revision 1.24  2005/05/19 13:02:02  southa
 * Mac release work
 *
 * Revision 1.23  2004/01/06 20:46:50  southa
 * Build fixes
 *
 * Revision 1.22  2004/01/02 21:13:07  southa
 * Source conditioning
 *
 * Revision 1.21  2003/10/07 22:40:05  southa
 * Created MeshMover
 *
 */

#include "mushMushcore.h"

class GamePiece
{
public:
    virtual ~GamePiece() {}
};
//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
