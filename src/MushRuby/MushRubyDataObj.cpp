//%Header {
/*****************************************************************************
 *
 * File: src/MushRuby/MushRubyDataObj.cpp
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } pPY2CsH5p8HKWwKW2KYZnw
/*
 * $Id: MushRubyDataObj.cpp,v 1.2 2007/04/18 09:23:01 southa Exp $
 * $Log: MushRubyDataObj.cpp,v $
 * Revision 1.2  2007/04/18 09:23:01  southa
 * Header and level fixes
 *
 * Revision 1.1  2006/06/13 10:35:05  southa
 * Ruby data objects
 *
 */

#include "MushRubyDataObj.h"

