//%includeGuardStart {
#ifndef MUSHRUBYRUBY_H
#define MUSHRUBYRUBY_H
//%includeGuardStart } yjut4kNuvNgdox48b+y6Aw
//%Header {
/*****************************************************************************
 *
 * File: src/MushRuby/MushRubyRuby.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } iV8kL7ld5mYfRGEwXZW7YQ
/*
 * $Id: MushRubyRuby.h,v 1.2 2007/04/18 09:23:03 southa Exp $
 * $Log: MushRubyRuby.h,v $
 * Revision 1.2  2007/04/18 09:23:03  southa
 * Header and level fixes
 *
 * Revision 1.1  2006/04/20 00:22:45  southa
 * Added ruby executive
 *
 */

extern "C"
{
#include "ruby.h"
}
//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
