//%includeGuardStart {
#ifndef MUSHRUBY_H
#define MUSHRUBY_H
//%includeGuardStart } U/64qX501i9BXUFwLOFhbQ
//%Header {
/*****************************************************************************
 *
 * File: src/MushRuby/MushRuby.h
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } MEfBV8/wn7JjG3V/EPwQsA
/*
 * $Id: MushRuby.h,v 1.10 2007/04/18 09:23:01 southa Exp $
 * $Log: MushRuby.h,v $
 * Revision 1.10  2007/04/18 09:23:01  southa
 * Header and level fixes
 *
 * Revision 1.9  2006/06/20 19:06:54  southa
 * Object creation
 *
 * Revision 1.8  2006/06/16 12:11:05  southa
 * Ruby subclasses
 *
 * Revision 1.7  2006/06/14 11:20:09  southa
 * Ruby mesh generation
 *
 * Revision 1.6  2006/06/13 19:30:39  southa
 * Ruby mesh generation
 *
 * Revision 1.5  2006/06/13 10:35:05  southa
 * Ruby data objects
 *
 * Revision 1.4  2006/06/12 11:59:40  southa
 * Ruby wrapper for MushMeshVector
 *
 * Revision 1.3  2006/04/21 00:10:43  southa
 * MushGLFont ruby module
 *
 * Revision 1.2  2006/04/20 00:22:45  southa
 * Added ruby executive
 *
 * Revision 1.1  2006/04/19 20:21:34  southa
 * Added Ruby framework
 *
 */

#include "MushRubyDataObj.h"
#include "MushRubyEmptyObj.h"
#include "MushRubyExec.h"
#include "MushRubyFail.h"
#include "MushRubyInstall.h"
#include "MushRubyIntern.h"
#include "MushRubyMaptorObj.h"
#include "MushRubyObj.h"
#include "MushRubyObject.h"
#include "MushRubyStandard.h"
#include "MushRubyUtil.h"



//%includeGuardEnd {
#endif
//%includeGuardEnd } hNb4yLSsimk5RFvFdUzHEw
