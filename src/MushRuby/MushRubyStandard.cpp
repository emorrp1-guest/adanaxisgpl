//%Header {
/*****************************************************************************
 *
 * File: src/MushRuby/MushRubyStandard.cpp
 *
 * Author: Andy Southgate 2002-2007
 *
 * This file contains original work by Andy Southgate.  The author and his
 * employer (Mushware Limited) irrevocably waive all of their copyright rights
 * vested in this particular version of this file to the furthest extent
 * permitted.  The author and Mushware Limited also irrevocably waive any and
 * all of their intellectual property rights arising from said file and its
 * creation that would otherwise restrict the rights of any party to use and/or
 * distribute the use of, the techniques and methods used herein.  A written
 * waiver can be obtained via http://www.mushware.com/.
 *
 * This software carries NO WARRANTY of any kind.
 *
 ****************************************************************************/
//%Header } mRCdQWieMEI+fKtD6Z4dDA
/*
 * $Id: MushRubyStandard.cpp,v 1.5 2007/04/18 09:23:03 southa Exp $
 * $Log: MushRubyStandard.cpp,v $
 * Revision 1.5  2007/04/18 09:23:03  southa
 * Header and level fixes
 *
 * Revision 1.4  2006/06/16 12:11:05  southa
 * Ruby subclasses
 *
 * Revision 1.3  2006/06/13 10:35:06  southa
 * Ruby data objects
 *
 * Revision 1.2  2006/04/21 00:10:43  southa
 * MushGLFont ruby module
 *
 */

#include "MushRubyStandard.h"

#include "MushRubyRuby.h"

namespace Mushware
{
	const Mushware::tRubyValue kRubyQnil = Qnil;
	const Mushware::tRubyValue kRubyQfalse = Qfalse;
	const Mushware::tRubyValue kRubyQtrue = Qtrue;	
}
